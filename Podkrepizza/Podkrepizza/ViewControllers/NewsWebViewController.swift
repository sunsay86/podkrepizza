//
//  NewsWebViewController.swift
//  Podkrepizza
//
//  Created by Александр Волков on 07.02.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class NewsWebViewController: UIViewController, UIWebViewDelegate {

    
    
    
    //MARK: Outlets
    @IBOutlet weak var webView: UIWebView! {
        didSet {
            if URL != nil {
                webView.delegate = self
                //self.title = URL!.host
                webView.scalesPageToFit = true
                webView.loadRequest(URLRequest(url: URL! as URL))
            }
        }
    }
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    //MARK: Public API
    
    var URL = NSURL(string: "https://vk.com/podkrepizzaclub")
    
    
    //MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //navigationItem.leftBarButtonItem = UIBarButtonItem    

    }
//MARK: UIWebViewDelegate
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        spinner.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        spinner.stopAnimating()
        spinner.hidesWhenStopped = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        spinner.stopAnimating()
        print("it was problem to load web page")
    }
    /*
     
     
     
     
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
