//
//  ViewController.swift
//  Podkrepizza
//
//  Created by Александр Волков on 18.10.16.
//  Copyright © 2016 Александр Волков. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    

        
    @IBAction func Login(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "Login" {
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc : UIViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarViewController
            let window :UIWindow = UIApplication.shared.keyWindow!
            window.rootViewController = vc;
            window.makeKeyAndVisible()
            
            
        }
    }
    
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print ("LoginviewDidLoad")
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    
   override func viewWillAppear(_ animated: Bool) {print ("LoginviewWillAppear")}// Called when the view is about to made visible. Default does nothing
    
     override func viewDidAppear(_ animated: Bool) {print ("LoginviewDidAppear")}// Called when the view has been fully transitioned onto the screen. Default does nothing
    
    override func viewWillDisappear(_ animated: Bool) {print ("LoginviewWillDisappear")} // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        print ("bye Login view")
    }
    
    
   
    deinit {
        print ("deinit LoginVC")
    }
  */
}

