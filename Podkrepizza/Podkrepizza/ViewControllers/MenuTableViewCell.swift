//
//  MenuTableViewCell.swift
//  Podkrepizza
//
//  Created by Александр Волков on 23.02.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
    
    var menu: Product? {
        didSet{
            updateUI()
        }
    }
    private func updateUI() {
        if let menu = self.menu {
            imageObject.image = menu.image
            name.text = menu.name
            price.text = menu.price
            ingridients.text = menu.ingridients
        }
        
    }
    @IBOutlet weak var imageObject: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var ingridients: UILabel!
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
