//
//  MenuModel.swift
//  Podkrepizza
//
//  Created by Александр Волков on 03.03.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

struct Product {
    let name: String
    let price: String
    let image: UIImage
    let ingridients: String
}

class MenuModel {
        
    open var objects: [Product] = [
        Product(name: "Местная",
                price: "450 ₽",
                image: #imageLiteral(resourceName: "Mestnaya"),
                ingridients:
            "Курица с гриля, бекон, пепперони, свиной окорок, корнишоны, шампиньоны, томат, перец болгарский, чесночный соус, сыр моцарелла."),
        Product(name: "Пепперони",
                price: "420 ₽",
                image: #imageLiteral(resourceName: "Pepperoni"),
                ingridients:
            "Пепперони, ветчина, томат. перчики халапенью, корнишоны, маслинв, томатный мутти соус, сыр моцарелла."),
        Product(name: "Баварская",
                price: "420 ₽",
                image: #imageLiteral(resourceName: "Bavarskaya"),
                ingridients:
            "Баварские колбаски, копченая курица, корнишоны, бекон, свиной окорок, шампиньоны, томатный мутти соус, сыр моцарелла."),
        Product(name: "Царь",
                price: "480 ₽",
                image: #imageLiteral(resourceName: "Tsar"),
                ingridients:
            "Куриное филе, копченая курица, баварские колбаски, свиной окорок, свиная вырезка, пепперони, огурцы маринованные, лук-шалот, шампиньоны, соус чесночный, сыр моцарелла."),
        Product(name: "Ранчо",
                price: "450 ₽",
                image: #imageLiteral(resourceName: "Rancho"),
                ingridients:
            "Баварские колбаски, копченая курица, бекон, куриное филе, томат, перец болгарский, соус чесночный, сыр моцарелла."),
        Product(name: "Мясная гриль",
                price: "450 ₽",
                image: #imageLiteral(resourceName: "myaso grill"),
                ingridients:
            "Мясо с гриля, свиной окорок, шампиньоны, томат, корнишоны, соус чесночный, сыр моцарелла."),
        Product(name: "Лас Вегас",
                price: "420 ₽",
                image: #imageLiteral(resourceName: "Las Vegas"),
                ingridients:
            "Куриное филе, копченая курица, пепперони, бекон, шампиньоны, томат, корнишоны, лук-шалот, соус чесночный, сыр моцарелла."),
        Product(name: "Гурман",
                price: "450 ₽",
                image: #imageLiteral(resourceName: "Gurman"),
                ingridients:
            "Ветчина, свиная вырезка, бекон, куриное филе, корнишоны, соус чесночный, сыр моцарелла"),
        Product(name: "Ветчина бекон",
                price: "310 ₽",
                image: #imageLiteral(resourceName: "vetchina becon"),
                ingridients:
            "Ветчина, шампиньоны, томат, томатный мутти соус, сыр моцарелла"),
        Product(name: "Ветчина грибы",
                price: "330 ₽",
                image: #imageLiteral(resourceName: "vetchina grib"),
                ingridients:
            "Ветчина, бекон, томат, чесночный соус, сыр моцарелла.")
        
        
    ]
    
    
}

