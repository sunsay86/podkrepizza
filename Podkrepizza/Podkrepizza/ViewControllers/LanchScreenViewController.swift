//
//  LanchScreenViewController.swift
//  Podkrepizza
//
//  Created by Александр Волков on 16.12.16.
//  Copyright © 2016 Александр Волков. All rights reserved.
//

import UIKit

class LanchScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {print ("LaunchviewWillAppear")}// Called when the view is about to made visible. Default does nothing
    
    override func viewDidAppear(_ animated: Bool) {print ("LaunchviewDidAppear")}// Called when the view has been fully transitioned onto the screen. Default does nothing
    
    override func viewWillDisappear(_ animated: Bool) {print ("LAunchviewWillDisappear")} // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        print ("bye Launch view")
    }
    
    
    
    deinit {
        print ("deinit LaunchVC")
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
